import { TestBed } from '@angular/core/testing';

import { GitApiService } from './git-api.service';

describe('GitApiServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GitApiService = TestBed.get(GitApiService);
    expect(service).toBeTruthy();
  });
});

import { Issue } from './../classes/model/issue';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GitApiService {
  readonly  headerclaveAPI:string = "PRIVATE-TOKEN";
  readonly  valorClaveAPI:string = "_zFT8x6kfxEdMWAAF3Y-";
  readonly  projectId:string = "9959053";
  readonly   baseUrl:string  = "https://gitlab.com/api/v4/projects/" + this.projectId + "/issues";
  private  cabeceras:HttpHeaders = new HttpHeaders().set(this.headerclaveAPI, this.valorClaveAPI);

  getTareas() {
    return this.http.get<Array<Issue>>(this.baseUrl, {headers: this.cabeceras});
  }

  getTarea(iid:string) {
    console.log(iid);
    return this.http.get<Issue>(this.baseUrl + '/' + iid, {headers: this.cabeceras});
  }

  newTarea(tarea:Issue) {
    return this.http.post<Issue>(this.baseUrl, tarea, {headers: this.cabeceras});
  }

  editarTarea(tarea:Issue) {
    console.log('Estamos en editar tarea ----------');    
    //return this.http.put<Issue>(this.baseUrl +'/' + tarea.iid, JSON.stringify(tarea), {headers: this.cabeceras});
    return this.http.put<Issue>(this.baseUrl +'/' + tarea.iid, {title:tarea.title, description: tarea.description}, {headers: this.cabeceras});
  }

  borrarTarea(iid:number) {
    return this.http.delete<Issue>(this.baseUrl +'/' + iid, {headers: this.cabeceras});
  }


  constructor(private http:HttpClient ) { }
}

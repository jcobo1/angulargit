
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './views/login/login.component';
import { HeaderComponent } from './views/header/header.component';
import { FooterComponent } from './views/footer/footer.component';
import { ListadoComponent } from './views/listado/listado.component';
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './views/home/home.component';

import { DetailComponent } from './views/detail/detail.component';

import { NewTareaComponent } from './views/new-tarea/new-tarea.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    ListadoComponent,
    HomeComponent,
    DetailComponent,
    NewTareaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { GitApiService } from './../../services/git-api.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Issue } from 'src/app/classes/model/issue';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  constructor(_router:Router,public _servicio:GitApiService) { }
  tareas:Array<Issue> = [];
  tarEliminarId:number;

  ngOnInit() {
    this._servicio.getTareas()
    .subscribe(
      result => {
        this.tareas = result;
        console.log(this.tareas);
      },
      error => {
        if (error !== null) {
          console.log("Error GetIrregulares: ");
          console.log(error);
          // this.mensajeError = error.msg;
        } 
      }
    )
  }
  borrar(iid:number){
      this._servicio.borrarTarea(iid).subscribe(
        result => {
          console.log('Borrado el registro ' + iid);
          this.ngOnInit();
        }
      );
  }

  tareasEliminarId(iid:number){
    this.tarEliminarId=iid;
  }
}

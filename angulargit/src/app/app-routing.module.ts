import { HomeComponent } from './views/home/home.component';
import { ListadoComponent } from './views/listado/listado.component';
import { LoginComponent } from './views/login/login.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from './views/detail/detail.component';
import { NewTareaComponent } from './views/new-tarea/new-tarea.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent,
    children: [
      {path:'', component:ListadoComponent},
      {path:'nuevaTarea', component:NewTareaComponent},
      {path:'detalleTarea/:id', component:DetailComponent},
      {path:'editarTarea/:id', component:NewTareaComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

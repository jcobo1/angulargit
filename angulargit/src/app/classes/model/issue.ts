import { Author } from './author';
export class Issue {
        public id: number;
        public iid: number;
        public title: string;
        public description: string;
        public upvotes: number;
        public downvotes: number;
        public author:  Author = new Author;        
        public created_at : any;
        public updated_at: Date;
        public assignee : any; 
        public labels: any[];
}
